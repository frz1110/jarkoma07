#!/usr/bin/env python

import socket

SERVER_IP = "184.73.121.201"
SERVER_PORT = 8534
BUFFER_SIZE = 1024

def main():
    sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sc.connect((SERVER_IP, SERVER_PORT))

    print("Example Socket Client Program")

    input_value = input("Enter a string to send to the server: ")
    input_value_bytes = input_value.encode("UTF-8")
    sc.send(input_value_bytes)

    output_value_bytes = sc.recv(BUFFER_SIZE)
    output_value = output_value_bytes.decode("UTF-8")
    print(output_value)

    sc.close()

if __name__ == "__main__":
    main()
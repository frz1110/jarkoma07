#!/usr/bin/env python

import socket
import time
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

SERVER_IP = "127.0.0.1"
SERVER_PORT = 5455
BUFFER_SIZE = 1024

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sc:
        sc.bind((SERVER_IP, SERVER_PORT))
        sc.listen(0)

        # Tegur Sapa
        connection, address = sc.accept()
        input_value_bytes = connection.recv(BUFFER_SIZE)
        input_value = input_value_bytes.decode("UTF-8")
        print(input_value)

        output_value = "Halo Instance A!"
        output_value_enc = output_value.encode("UTF-8")
        connection.send(output_value_enc)
        
        # Receive Symmetric Key
        input_text = connection.recv(BUFFER_SIZE)
        print(input_text.decode('UTF-8'))
        input_file = connection.recv(BUFFER_SIZE)

       
        key = input_file
        cipher = AES.new(key, AES.MODE_ECB)
    
        data = 'Symmetric Key diterima'
        ciphertext = cipher.encrypt(pad(data.encode(),16))
        print(ciphertext)
        connection.send(ciphertext)
        connection.close()

if __name__ == "__main__":
    main()
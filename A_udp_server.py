import re
import socket
import time

from typing import Tuple


BUFFER_SIZE = 2048
SERVER_IP = ""
SERVER_PORT = 8534

def logic(input: str):
    time.sleep(10)
    found = re.search(r'[Jj]arkom', input)

    return "Match found" if found else "Match not found"


def socket_handler(sc: socket.socket, inbound_message_raw: bytearray, source_addr: Tuple[str, int]):
    inbound_message = inbound_message_raw.decode("UTF-8")

    print(f"[INPUT] [SRC: {source_addr}] [TIME: {time.asctime()}]: {inbound_message}")

    outgoing_message = logic(inbound_message)
    outgoing_message_raw = outgoing_message.encode("UTF-8")
    sc.sendto(outgoing_message_raw, source_addr)

    print(f"[OUTPUT] [DST: {source_addr}] [TIME: {time.asctime()}]: {outgoing_message}")

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as server_socket:
        server_socket.bind((SERVER_IP, SERVER_PORT))
        while True:
            inbound_message_raw, source_addr = server_socket.recvfrom(BUFFER_SIZE)
            socket_handler(server_socket, inbound_message_raw, source_addr)

if __name__ == "__main__":
    main()
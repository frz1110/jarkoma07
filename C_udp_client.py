import socket

BUFFER_SIZE = 2048
SERVER_IP = "184.73.121.201"
SERVER_PORT = 8534

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    outgoing_message = input("Please enter message to be sent to server: ")
    outgoing_message_raw = outgoing_message.encode("UTF-8")

    client_socket.sendto(outgoing_message_raw, (SERVER_IP, SERVER_PORT))

    inbound_message_raw, source_addr = client_socket.recvfrom(BUFFER_SIZE)
    inbound_message = inbound_message_raw.decode("UTF-8")

    print(f"Reply from server {source_addr}: {inbound_message}")

    client_socket.close()

if __name__ == "__main__":
    main()
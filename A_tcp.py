#!/usr/bin/env python

import socket
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

SERVER_IP = "127.0.0.1"
SERVER_PORT = 5455
BUFFER_SIZE = 1024

# def insisiasi():
#     with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sc:
#         sc.bind((SERVER_IP, SERVER_PORT))
#         sc.listen(0)

def main():
    initiator = input('Do you want to be the initator (yes/no)?')

    #if initiator == 'yes':
    
    
    sc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sc.connect((SERVER_IP, SERVER_PORT))

    # Tegur Sapa
    input_value = "Halo Instance B!"
    input_value_bytes = input_value.encode("UTF-8")
    sc.send(input_value_bytes)

    output_value = sc.recv(BUFFER_SIZE)
    output_value_dec = output_value.decode("UTF-8")
    print(output_value_dec)

    # Send Symmetric Key
    key = "Ini Symmetric Keynya"
    key_send = key.encode("UTF-8")
    sc.send(key_send)

    file_data = open('A_SymmetricKey.txt', "rb")
    data = file_data.read(BUFFER_SIZE)
    sc.send(data)

    output_value = sc.recv(BUFFER_SIZE)
    cipher = AES.new(data, AES.MODE_ECB)
    output_value_dec = cipher.decrypt(output_value).decode()
    print(output_value_dec)

    sc.close()

if __name__ == "__main__":
    main()